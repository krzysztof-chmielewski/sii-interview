package pl.adriankozlowski.printer.api;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import pl.adriankozlowski.IntegrationTest;

import static org.assertj.core.api.Assertions.assertThat;

class PrinterTest extends IntegrationTest {
    @Autowired
    private Printer printer;
    @Autowired
    private ApplicationContext applicationContext;

    @Test
    void documentsAreQueued() {
        scheduleForPrinting(1L, "title", "content");
        scheduleForPrinting(2L, "title2", "content2");

        assertThat(printer.queue()).hasSize(2);
    }

    @Test
    void printerFollowsFifoAlgorithm() {
        scheduleForPrinting(1L, "title", "content");
        scheduleForPrinting(2L, "title2", "content2");
        scheduleForPrinting(3L, "title3", "content3");

        printer.printNextPrintableDocument();

        assertThat(printer.queue()).containsExactly(
                new PrintableDocument(2L, "title2", "content2"),
                new PrintableDocument(3L, "title3", "content3")
        );
    }

    @Test
    void documentIsRemovedFromQueueIfNothingIsBeingPrinted() {
        scheduleForPrinting(1L, "title", "content");
        scheduleForPrinting(2L, "title2", "content2");
        scheduleForPrinting(3L, "title3", "content3");

        scheduleForRemoval(2L);

        assertThat(printer.queue()).containsExactly(
                new PrintableDocument(1L, "title", "content"),
                new PrintableDocument(3L, "title3", "content3")
        );
    }

    @Test
    void documentIsRemovedFromQueueIfOtherIsBeingPrinted() {
        scheduleForPrinting(1L, "title", "content");
        scheduleForPrinting(2L, "title2", "content2");
        scheduleForPrinting(3L, "title3", "content3");

        printer.beingPrinted = new PrintableDocument(1L, "title2", "content2");

        scheduleForRemoval(2L);

        assertThat(printer.queue()).containsExactly(
                new PrintableDocument(1L, "title", "content"),
                new PrintableDocument(3L, "title3", "content3")
        );
    }

    @Test
    void removingNonExistingDocumentDoesNotAlterQueue() {
        scheduleForPrinting(1L, "title", "content");
        scheduleForPrinting(2L, "title2", "content2");
        scheduleForPrinting(3L, "title3", "content3");

        scheduleForRemoval(4L);

        assertThat(printer.queue()).containsExactly(
                new PrintableDocument(1L, "title", "content"),
                new PrintableDocument(2L, "title2", "content2"),
                new PrintableDocument(3L, "title3", "content3")
        );
    }

    @Test
    void cannotRemoveDocumentBeingPrinted() {
        scheduleForPrinting(1L, "title", "content");
        printer.beingPrinted = new PrintableDocument(1L, "title", "content");

        scheduleForRemoval(1L);

        assertThat(printer.queue()).containsExactly(
                new PrintableDocument(1L, "title", "content")
        );
    }

    private void scheduleForPrinting(long id, String title, String content) {
        applicationContext.publishEvent(new PrintingRequest(this, new PrintableDocument(id, title, content)));
    }

    private void scheduleForRemoval(long id) {
        applicationContext.publishEvent(new RemoveRequest(this, id));
    }
}