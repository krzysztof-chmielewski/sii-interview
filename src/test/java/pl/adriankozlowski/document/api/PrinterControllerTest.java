package pl.adriankozlowski.document.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pl.adriankozlowski.IntegrationTest;

import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class PrinterControllerTest extends IntegrationTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void initialQueueIsEmpty() throws Exception {
        var queue = listQueue();

        assertThat(queue).isEmpty();
    }

    @Test
    void idIsGeneratedForAddedDocument() throws Exception {
        var id = queueDocument(new DocumentData("title", "content"));

        assertThat(id).isEqualTo(1L);
    }

    @Test
    void queueIsFilledWithAddedDocuments() throws Exception {
        queueDocument(new DocumentData("title", "content"));
        queueDocument(new DocumentData("title2", "content2"));

        var queue = listQueue();

        assertThat(queue).containsExactly(
                new DocumentData(1L, "title", "content"),
                new DocumentData(2L, "title2", "content2")
        );
    }

    @Test
    void documentsCanBeRemoved() throws Exception {
        queueDocument(new DocumentData("title", "content"));
        queueDocument(new DocumentData("title2", "content2"));
        queueDocument(new DocumentData("title3", "content3"));
        removeDocument(1L);
        removeDocument(2L);

        var queue = listQueue();

        assertThat(queue).containsExactly(
                new DocumentData(3L, "title3", "content3")
        );
    }

    private void removeDocument(Long id) throws Exception {
        mockMvc.perform(delete("/printer/" + id).headers(json()))
                .andExpect(status().isOk())
                .andReturn();
    }

    private Long queueDocument(DocumentData documentData) throws Exception {
        var body = objectMapper.writeValueAsBytes(documentData);
        var response = mockMvc.perform(post("/printer").content(body).headers(json()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsByteArray();

        return objectMapper.readValue(response, Long.class);
    }

    private Collection<DocumentData> listQueue() throws Exception {
        var response = mockMvc.perform(get("/printer/list").headers(json()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsByteArray();

        return objectMapper.readValue(response, new TypeReference<>() {
        });
    }

    private HttpHeaders json() {
        var headers = new HttpHeaders();
        headers.add("Content-type", MediaType.APPLICATION_JSON_VALUE);
        return headers;
    }
}