package pl.adriankozlowski.printer.internal;

import org.springframework.stereotype.Service;
import pl.adriankozlowski.printer.api.PrintableDocument;

import static com.google.common.base.Preconditions.checkNotNull;

@Service
public class PrintingService {
    public void print(PrintableDocument document) {
        checkNotNull(document, "Document cannot be null");
        System.out.println("------------------------------------------");
        System.out.println("--- Printing document with id: " + document.getId());
        System.out.println("--- " + document.getTitle());
        try {
            Thread.sleep(1000 + document.getContent().length() * 100L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("--- " + document.getContent());
        System.out.println("------------------------------------------");
    }
}
