package pl.adriankozlowski.printer.api;

import org.springframework.context.ApplicationEvent;

import static com.google.common.base.Preconditions.checkNotNull;

public class PrintingRequest extends ApplicationEvent {
    private final PrintableDocument printableDocument;

    public PrintingRequest(Object source, PrintableDocument printableDocument) {
        super(source);
        this.printableDocument = checkNotNull(printableDocument, "Printable document cannot be null");
    }

    public PrintableDocument getPrintableDocument() {
        return printableDocument;
    }
}
