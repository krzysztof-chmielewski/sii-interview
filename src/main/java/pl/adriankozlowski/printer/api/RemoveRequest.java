package pl.adriankozlowski.printer.api;

import org.springframework.context.ApplicationEvent;

import static com.google.common.base.Preconditions.checkNotNull;

public class RemoveRequest extends ApplicationEvent {
    private final Long id;

    public RemoveRequest(Object source, Long id) {
        super(source);
        this.id = checkNotNull(id, "Id document cannot be null");
    }

    public Long getId() {
        return id;
    }
}
