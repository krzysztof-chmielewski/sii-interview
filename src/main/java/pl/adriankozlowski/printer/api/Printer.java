package pl.adriankozlowski.printer.api;

import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.adriankozlowski.printer.internal.PrintingService;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

@Component
public class Printer {
    private final PrintingService printingService;
    private final Queue<PrintableDocument> queue = new ConcurrentLinkedQueue<>();
    PrintableDocument beingPrinted;

    public Printer(PrintingService printingService) {
        this.printingService = printingService;
    }

    public Queue<PrintableDocument> queue() {
        return new LinkedList<>(queue);
    }

    @EventListener(PrintingRequest.class)
    public void offer(PrintingRequest event) {
        queue.offer(event.getPrintableDocument());
    }

    @EventListener(RemoveRequest.class)
    public void remove(RemoveRequest event) {
        if (anyDocumentIsBeingPrinted()) {
            if (scheduledForRemovalIsBeingPrinted(event)) {
                System.out.println("Cannot remove from queue as document with id `" + event.getId() + "` is already being printed");
            } else {
                tryRemoveDocument(event);
            }
        } else {
            tryRemoveDocument(event);
        }
    }

    @Scheduled(fixedRate = 100)
    public void printNextPrintableDocument() {
        if (!queue.isEmpty()) {
            if (beingPrinted == null) {
                beingPrinted = queue.poll();
                printingService.print(beingPrinted);
                beingPrinted = null;
            }
        }
    }

    private boolean scheduledForRemovalIsBeingPrinted(RemoveRequest event) {
        return beingPrinted.getId().equals(event.getId());
    }

    private boolean anyDocumentIsBeingPrinted() {
        return beingPrinted != null;
    }

    private void tryRemoveDocument(RemoveRequest event) {
        if (successfullyRemovedDocument(event)) {
            System.out.println("Successfully removed document with id `" + event.getId() + "` from queue.");
        } else {
            System.out.println("Cannot remove from queue document with id `" + event.getId() + "` as it does not exist.");
        }
    }

    private boolean successfullyRemovedDocument(RemoveRequest event) {
        return queue.removeIf(printableDocument -> printableDocument.getId().equals(event.getId()));
    }
}
