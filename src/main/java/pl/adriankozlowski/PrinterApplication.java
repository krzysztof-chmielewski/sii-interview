package pl.adriankozlowski;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;


@SpringBootApplication
@Import(SchedulingConfig.class)
public class PrinterApplication {
    public static void main(String[] args) {
        SpringApplication.run(PrinterApplication.class);
    }
}
