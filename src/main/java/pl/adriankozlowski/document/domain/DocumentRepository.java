package pl.adriankozlowski.document.domain;

import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Repository
class DocumentRepository {
    private final Map<Long, Document> documents = new ConcurrentHashMap<>();
    private final AtomicLong currentId = new AtomicLong(1L);

    public Long save(Document document) {
        var id = currentId.getAndIncrement();
        documents.put(id, new Document(id, document.getTitle(), document.getContent()));
        return id;
    }

    public Collection<Document> fetchAll() {
        return documents.values();
    }

    public void remove(Long id) {
        documents.remove(id);
    }
}
