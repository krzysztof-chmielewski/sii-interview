package pl.adriankozlowski.document.domain;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import pl.adriankozlowski.document.api.DocumentData;
import pl.adriankozlowski.printer.api.PrintableDocument;
import pl.adriankozlowski.printer.api.Printer;
import pl.adriankozlowski.printer.api.PrintingRequest;
import pl.adriankozlowski.printer.api.RemoveRequest;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class DocumentService {
    private final DocumentRepository documentRepository;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final Printer printer;

    public DocumentService(DocumentRepository documentRepository, ApplicationEventPublisher applicationEventPublisher, Printer printer) {
        this.documentRepository = documentRepository;
        this.applicationEventPublisher = applicationEventPublisher;
        this.printer = printer;
    }

    public Queue<DocumentData> printingQueue() {
        return printer.queue().stream()
                .map(fromPrintableDocument())
                .collect(Collectors.toCollection(LinkedList::new));
    }

    public Collection<DocumentData> getAll() {
        return documentRepository.fetchAll().stream()
                .map(fromEntity())
                .collect(Collectors.toCollection(LinkedList::new));
    }

    public Long add(DocumentData documentData) {
        var document = toEntity(documentData);
        var id = documentRepository.save(document);
        applicationEventPublisher.publishEvent(new PrintingRequest(this, toPrintableDocument(id, document)));
        return id;
    }

    private PrintableDocument toPrintableDocument(Long id, Document document) {
        return new PrintableDocument(id, document.getTitle(), document.getContent());
    }

    private Function<Document, DocumentData> fromEntity() {
        return document -> new DocumentData(document.getId(), document.getTitle(), document.getContent());
    }

    private Function<PrintableDocument, DocumentData> fromPrintableDocument() {
        return document -> new DocumentData(document.getId(), document.getTitle(), document.getContent());
    }

    private Document toEntity(DocumentData document) {
        return new Document(document.getTitle(), document.getContent());
    }

    public void remove(Long id) {
        documentRepository.remove(id);
        applicationEventPublisher.publishEvent(new RemoveRequest(this, id));
    }
}
