package pl.adriankozlowski.document.api;

import org.springframework.web.bind.annotation.*;
import pl.adriankozlowski.document.domain.DocumentService;

import java.util.Collection;


@RestController
@RequestMapping("printer")
public class PrinterController {
    private final DocumentService documentService;

    public PrinterController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @GetMapping("list")
    public Collection<DocumentData> list() {
        return documentService.printingQueue();
    }

    @PostMapping
    public Long add(@RequestBody DocumentData document) {
        return documentService.add(document);
    }

    @DeleteMapping("{id}")
    public void remove(@PathVariable Long id) {
        documentService.remove(id);
    }
}
