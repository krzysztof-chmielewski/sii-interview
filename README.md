# O co chodzi?
Sklonuj repozytorium i zaimplementuj.
Piszemy aplikację webową, która wystawia API restowe. Służy ona do obsługi serwera drukowania.
Załóżmy, że proces drukowania jest niezależny od API (nie wymuszamy drukowania poprzez api). 
Sprawdza on czy dokument znajduje się na w kolejce, ściąga taki dokument 
z kolejki i następnie drukuje go(dla uproszczenia w konsoli).
Dla uproszczenia ewentualną bazę danych zastąpmy kolekcją.
Używając __dowolnych__ narzędzi należy utworzyć następuące endpointy:
### 1 GET /printer/list
Endpoint pokazujący w jsonie listę zakolejkowanych dokumentów
### 2 POST /printer
Endpoint przyjmujący dokument jako ciało żądania. Dokument jest ustawiany jako ostani do wydrukowania. W odpowiedzi powinien zwrócić identyfikator zakolejkowanego dokumentu.
### 3 DELETE /printer/{id}
Endpoint usuwa dokument z kolejki drukowania
### 4 proces drukowania
zgodnie z powyższym zaimplementuj proces drukowania w konsoli
